/* jshint node: true */
/* jshint esversion: 6 */
"use strict";

const rp =    require('request-promise');
const http =  require('./http');
const utils = require('./utils');

var params = {};

var init = (p) => {
  params = p;
};

/**
 * Create tracking links
 *
 * @param string url
 * @param integer adspace
 * @return void
 */
var create = (url) => {
  return new Promise((resolve, reject) => {
    var _url = `${http.getServiceUrl(params.environment, params.token)}/deeplink/_create?sourceId=${params.sourceid}&url=${utils.urlencode(url)}`;

    rp.get(_url)
      .then(body => {
        var resp = JSON.parse(body);

        if (resp.requestInfo.status !== 'OK') {
          reject({
            "msg": resp.message
          }, "");
        } else if (resp.deeplinks[0].status == true) {
          resolve(resp.deeplinks[0].deeplink);
        } else if (resp.deeplinks[0].status == false) {
          reject({
            "msg": resp.deeplinks[0].message
          }, "");
        } else {
          reject({
            "msg": "Invalid link to this program."
          }, "");
        }
      })
      .catch(error => {
        reject(error, null);
      });
  });
};

module.exports = {
  create: create,
  init: init
};

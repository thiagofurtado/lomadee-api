/* jshint node: true */
/* jshint esversion: 6 */
"use strict";

const rp = require('request-promise');
const http =  require('./http');
const utils = require('./utils');

var params = {};

var init = (p) => {
  params = p;
};

/**
 * Coupons API
 *
 * @see https://developer.lomadee.com/afiliados/cupons/recursos/buscar-cupons/
 * @param object params
 * categoryId: Filter by Category ID
 * storeId:    Filter by Store ID
 * keyword:    Filter by Keyword
 * @return Promise result
 */
var allCoupons = (queryParams = {}) => {
  return new Promise((resolve, reject) => {
    let query = `?sourceId=${params.sourceid}`;
    if (queryParams.categoryId) {
      query = query + `&categoryId=${queryParams.categoryId}`;
    }
    if (queryParams.storeId) {
      query = query + `&storeId=${queryParams.storeId}`;
    }
    if (queryParams.keyword) {
      query = query + `&keyword=${queryParams.keyword}`;
    }

    let _url = `${http.getServiceUrl(params.environment, params.token)}/coupon/_all${query}`;
    rp.get(_url)
      .then(body => {
        var resp = JSON.parse(body);
        resolve(resp);
      })
      .catch(error => {
        reject(error);
      });
  });
};

/**
 * Coupons API
 *
 * @see https://developer.lomadee.com/afiliados/cupons/recursos/buscar-cupons/
 * @param object params
 * categoryId: Filter by Coupon ID
 * @return Promise result
 */
var couponById = (id) => {
  return new Promise((resolve, reject) => {
    let _url = `${http.getServiceUrl(params.environment, params.token)}/coupon/_id/${id}?sourceId=${params.sourceid}`;
    rp.get(_url)
      .then(body => {
        var resp = JSON.parse(body);
        resolve(resp);
      })
      .catch(error => {
        reject(error);
      });
  });
};

module.exports = {
  all: allCoupons,
  coupon: couponById,
  init: init
};

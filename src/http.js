/* jshint node: true */
/* jshint esversion: 6 */
"use strict";

const SANDBOX_SERVICE = 'https://sandbox-api.lomadee.com/v2';
const SERVICE = 'https://api.lomadee.com/v2';

/**
 * Function to get service URL by environment (DEV/PROD)
 *
 * @return str
 */
function getServiceUrl(environment, appToken) {
  console.log(environment);
  console.log(appToken);
  if (environment === 'production' || environment === 'prod') {
    return `${SERVICE}/${appToken}`;
  } else {
    return `${SANDBOX_SERVICE}/${appToken}`;
  }
}

module.exports.getServiceUrl = getServiceUrl;

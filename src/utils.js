/* jshint node: true */
/* jshint esversion: 6 */
"use strict";

/**
 * Function to encode URL
 *
 * @see http://locutus.io/php/url/urlencode/
 * @param str
 * @return str
 */
function urlencode(str) {
  str = (str + '');
  return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

module.exports.urlencode = urlencode;

/* jshint node: true */
/* jshint esversion: 6 */
/**
 * Lomadee Affiliated API interface for Node.js
 *
 * @author Thiago Furtado <tfmend@gmail.com>
 *
 * @see https://developer.lomadee.com/lab/
 */
 "use strict";

let Lomadee = require("./index.js"),
    lomadee = new Lomadee('dev', "15248475989001effe1d6", "35962244");

// lomadee.categories({}, (err, results) => {
//     console.log(results);
// });
//
// lomadee.programs((err, results) => {
//     console.log(results);
// });
//
// lomadee.offers({}, (err, results) => {
//     console.log(results);
// });
//
// lomadee.product({}, (err, results) => {
//     console.log(results);
// });
//
// lomadee.report("user", "pass", {startDate: '27052016', endDate: '16072016', eventStatus: 0, publisherId: 'your publisher id'}, (err, results) => {
//     if(err)
//         console.log(err);
//     else
//         console.log(results);
// });

lomadee.coupons.all().then(result => {
    console.log(result);
});

lomadee.coupons.coupon(3747).then(result => {
    console.log(result);
});

lomadee.deeplink.create("http://www.submarino.com.br/").then(link => {
  console.log(link);
});
